var models = require('../../database/models')

exports.index = async function (req, res, next) {
    const products = await models.Product.findAll()
    res.send(products)
}

exports.create = async function (req, res, next) {
    var product = models.Product.build(req.body)
    try {
        await product.validate()
    } catch (err) {
        res.send(err)
        return
    }
    product.save()
    res.send(product)
}

exports.show = async function (req, res, next) {
    var product = await models.Product.findByPk(req.params.id)
    if (product === null) {
        res.sendStatus(404)
    } else {
        res.send(product)
    }
}

exports.delete = async function (req, res, next) {
    var product = await models.Product.findByPk(req.params.id)
    if (product === null) {
        res.sendStatus(404)
    } else {
        product.destroy()
        res.send()
    }
}

exports.edit = async function (req, res, next) {
    var product = await models.Product.findByPk(req.params.id)
    if (product === null) {
        res.sendStatus(404)
    } else {
        try {
            product.price = req.body.price
            await product.validate()
        } catch (err) {
            res.send(err)
            return
        }
        product.save()
        res.send(product)
    }

}