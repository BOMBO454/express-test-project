module.exports = {
  "development": {
    "dialect": "postgres",
    "url": process.env.DB_CONNECTION
  },
  "test": {
    "dialect": "sqlite",
    "storage": process.env.DB_SQLITE
  },
}
