var express = require('express');
var router = express.Router();
var products = require('../app/controllers/products')

/* GET products listing. */
router.get('/products', products.index);
router.post('/products', products.create);
router.get('/products/:id', products.show);
router.delete('/products/:id', products.delete);
router.put('/products/:id', products.edit);

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;
